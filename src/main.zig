const std = @import("std");

const Code = struct {
    code: u8,
    explored: bool = false,
};

const Matrix = std.ArrayList(std.ArrayList(Code));

fn Sequence(comptime T: type) type {
    return struct {
        items: [256]T = undefined,
        len: usize = 0,
        fn push(self: *Sequence(T), code: T) void {
            self.items[self.len] = code;
            self.len += 1;
        }
        fn pop(self: *Sequence(T)) void {
            self.len -= 1;
        }

        fn contains(self: *const Sequence(T), other: *const Sequence(T)) bool {
            return std.mem.containsAtLeast(T, self.items[0..self.len], 1, other.items[0..other.len]);
        }

        fn copy_from(self: *Sequence(T), other: *Sequence(T)) void {
            @memcpy(self.items[0..other.len], other.items[0..other.len]);
            self.len = other.len;
        }

        fn clear(self: *Sequence(T)) void {
            self.len = 0;
        }

        fn overlaps(self: *const Sequence(T), other: *const Sequence(T)) ?isize {
            var overlap: ?isize = null;
            next: for (0..self.len) |si| {
                const o = for (0..other.len) |oi| {
                    const i = (other.len - 1) - oi;
                    if (si >= oi) {
                        if (other.items[i] == self.items[si - oi]) {
                            continue;
                        }
                        continue :next;
                    } else {
                        break oi;
                    }
                } else {
                    return @as(isize, @intCast(si - (other.len - 1)));
                };
                overlap = @as(isize, @intCast(si)) - @as(isize, @intCast(o));
            }

            return overlap;
        }

        fn print(self: *const Sequence(T)) !void {
            var stdout = std.io.getStdOut().writer();
            for (0..self.len) |i| {
                try stdout.print("{x} ", .{self.items[i]});
            }
            try stdout.print("\n", .{});
        }
    };
}

const Sequences = std.ArrayList(Sequence(u8));

fn dumpOverlaps(s: *const Sequences) void {
    for (s.items, 0..) |seq, i| {
        for (s.items, 0..) |oseq, j| {
            if (i == j) break;

            if (seq.overlaps(&oseq)) |overlap| {
                if (overlap < 0) {
                    oseq.printSeq();
                    for (0..@abs(overlap)) |_| {
                        std.debug.print("   ", .{});
                    }
                    seq.printSeq();
                } else {
                    seq.printSeq();
                    for (0..@abs(overlap)) |_| {
                        std.debug.print("   ", .{});
                    }
                    oseq.printSeq();
                }
            }
            std.debug.print("\n", .{});
        }
        std.debug.print("\n", .{});
    }
}

const Match = struct {
    sequence: Sequence(u8),
    moves: Sequence(usize),
    matches: usize,

    fn print(self: *const Match, seqs: *const Sequences) !void {
        var min_overlap: isize = 0;
        var stdout = std.io.getStdOut().writer();
        for (seqs.items) |seq| {
            if (self.sequence.overlaps(&seq)) |overlap| {
                min_overlap = @min(min_overlap, overlap);
            }
        }
        for (seqs.items) |seq| {
            if (self.sequence.overlaps(&seq)) |overlap| {
                for (0..@intCast(overlap + @as(isize, @intCast(@abs(min_overlap))))) |_| {
                    try stdout.print("   ", .{});
                }
                try seq.print();
            }
        }
        for (0..@abs(min_overlap)) |_| {
            try stdout.print("   ", .{});
        }
        try self.sequence.print();
        var i: usize = self.moves.len;
        while (i > 0) {
            i -= 1;
            try stdout.print("{d:0>2} ", .{self.moves.items[i]});
        }
        try stdout.print("\n", .{});
    }

    fn is_better(self: *const Match, other: *const Sequence(u8), matches: usize) bool {
        return switch (std.math.order(self.matches, matches)) {
            .lt => true,
            .eq => self.sequence.len > other.len,
            .gt => false,
        };
    }
};

const Breach = struct {
    matrix: Matrix,
    sequences: Sequences,
    buffer_size: u8,
    bestDepth: usize = 0,
    result: ?Match,
    checks: usize = 0,
    currSequence: Sequence(u8) = Sequence(u8){},
    currMoves: Sequence(usize) = Sequence(usize){},

    fn check_matches(b: *Breach) std.mem.Allocator.Error!usize {
        var matches: usize = 0;
        for (b.sequences.items) |seq| {
            if (b.currSequence.contains(&seq)) {
                matches += 1;
            }
        }
        if (matches > 0) {
            if (b.result) |*best| {
                if (best.is_better(&b.currSequence, matches)) {
                    best.matches = matches;
                    best.sequence.copy_from(&b.currSequence);
                    best.moves.copy_from(&b.currMoves);
                }
            } else {
                b.result = Match{
                    .sequence = Sequence(u8){},
                    .matches = matches,
                    .moves = Sequence(usize){},
                };
                b.result.?.sequence.copy_from(&b.currSequence);
                b.result.?.moves.copy_from(&b.currMoves);
            }
        }
        return matches;
    }

    fn check(b: *Breach, idx: usize, currDepth: usize, comptime dir: Direction) ChoiceResult {
        b.checks += 1;
        var maxMatches = b.check_matches() catch unreachable;
        if (currDepth == b.bestDepth or maxMatches == b.sequences.items.len) {
            return .{ .matches = maxMatches, .depth = currDepth };
        }
        switch (dir) {
            .Row => for (b.matrix.items[idx].items, 0..) |*item, i| {
                if (!item.explored) {
                    if (!b.update(item, i, currDepth, &maxMatches, .Col)) {
                        break;
                    }
                }
            },
            .Col => for (b.matrix.items, 0..) |*row, i| {
                var item = &row.items[idx];
                if (!item.explored) {
                    if (!b.update(item, i, currDepth, &maxMatches, .Row)) {
                        break;
                    }
                }
            },
        }

        return .{ .matches = maxMatches, .depth = b.bestDepth };
    }
    inline fn update(b: *Breach, item: *Code, idx: usize, currDepth: usize, maxMatches: *usize, comptime dir: Direction) bool {
        item.explored = true;
        b.currSequence.push(item.code);
        b.currMoves.push(idx);
        defer item.explored = false;
        defer b.currSequence.pop();
        defer b.currMoves.pop();
        const result = b.check(idx, currDepth - 1, dir);
        if (maxMatches.* < result.matches) {
            maxMatches.* = result.matches;
            b.bestDepth = result.depth;
        } else if (maxMatches.* == result.matches and b.bestDepth < result.depth) {
            b.bestDepth = result.depth;
        } else {
            return true;
        }
        if (b.bestDepth == currDepth - 1 and maxMatches.* == b.sequences.items.len) {
            return false;
        }
        return true;
    }
};

const Error = error{
    MatrixNonEqualRowLengths,
    MatrixNonSquare,
};

fn parseBreach(file: std.fs.File, buffer_size: u8, alloc: std.mem.Allocator) !Breach {
    var reader = file.reader();

    var breach = Breach{
        .buffer_size = buffer_size,
        .matrix = try Matrix.initCapacity(alloc, 16),
        .sequences = try Sequences.initCapacity(alloc, 4),
        .result = null,
    };
    var line_buffer: [128]u8 = undefined;
    var stdout = std.io.getStdOut().writer();
    while (reader.readUntilDelimiter(&line_buffer, '\n') catch null) |line| {
        if (line.len <= 1) {
            break;
        }

        var row = try std.ArrayList(Code).initCapacity(alloc, 16);
        var nodes = std.mem.tokenizeScalar(u8, line[0..], ' ');
        while (nodes.next()) |node| {
            const val = std.fmt.parseInt(u8, node[0..2], 16) catch |err| {
                switch (err) {
                    std.fmt.ParseIntError.InvalidCharacter => {
                        std.log.err("Invalid hex digit '{s}'", .{node});
                    },
                    else => {},
                }
                return err;
            };
            try stdout.print("{x} ", .{val});
            try row.append(Code{ .code = val });
        }
        try stdout.print("\n", .{});

        if (breach.matrix.items.len == 0 or breach.matrix.items[0].items.len == row.items.len) {
            try breach.matrix.append(row);
        } else {
            return Error.MatrixNonEqualRowLengths;
        }
    }
    try stdout.print("\n", .{});
    // std.debug.print("size: {d}\n\n", .{breach.matrix.items.len});
    if (breach.matrix.items.len != breach.matrix.items[0].items.len) {
        return Error.MatrixNonSquare;
    }

    breach.buffer_size = @min(buffer_size, breach.matrix.items.len * breach.matrix.items.len);

    while (reader.readUntilDelimiter(&line_buffer, '\n') catch null) |line| {
        if (line.len <= 1) {
            continue;
        }
        var nodes = std.mem.tokenizeScalar(u8, line[0..], ' ');
        var sequence = Sequence(u8){};
        while (nodes.next()) |node| {
            if (node.len <= 1) {
                break;
            }
            const val = try std.fmt.parseInt(u8, node[0..2], 16);
            sequence.push(val);
            try stdout.print("{x} ", .{val});
        }
        try breach.sequences.append(sequence);
        try stdout.print("\n", .{});
    }
    try stdout.print("\n", .{});

    return breach;
}

const ChoiceResult = struct {
    matches: usize,
    depth: usize,
};

const Direction = enum {
    Row,
    Col,
};
const Move = union(Direction) {
    Row: u8,
    Col: u8,
};

const MatchResult = struct {
    matches: usize,
    kept: bool,
};

fn solve(b: *Breach) !void {
    var max_sequence_len: usize = 1;
    for (b.sequences.items) |seq| {
        max_sequence_len = @max(max_sequence_len, seq.len);
    }

    for (max_sequence_len..b.buffer_size) |maxDepth| {
        const result = b.check(0, maxDepth, .Row);
        if (b.bestDepth < result.depth) {
            b.bestDepth = result.depth;
        }
        if (result.matches == b.sequences.items.len) {
            break;
        }
    }

    std.debug.print("\n", .{});
    if (b.result) |match| {
        try match.print(&b.sequences);
    } else {
        std.debug.print("No matches found", .{});
    }
}

pub fn main() !void {
    var timer = try std.time.Timer.start();
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = arena.allocator();

    var buffer_size: u8 = 255;
    var matrix_file: std.fs.File = std.io.getStdIn();
    {
        const args = try std.process.argsAlloc(alloc);
        defer std.process.argsFree(alloc, args);
        defer _ = arena.reset(.retain_capacity);
        if (args.len > 1) {
            for (args[1..], 0..) |arg, i| {
                if (i > 1) {
                    std.log.err("Too many args supplied, only accept buffer size and/or filepath", .{});
                    return;
                }
                buffer_size = std.fmt.parseInt(u8, arg, 10) catch {
                    const filename = std.fs.path.resolve(alloc, &[_][]u8{arg}) catch {
                        std.log.err("Failed to parse buffer size or resolve path to file: arg = `{s}`", .{arg});
                        return;
                    };
                    matrix_file = try std.fs.cwd().openFile(filename, .{});
                    continue;
                };
            }
        }
    }
    std.debug.print("buffer size: {d}\n", .{buffer_size});
    const parse_args_time = timer.lap();
    var breach = try parseBreach(matrix_file, buffer_size, alloc);
    const read_breach_time = timer.lap();

    try solve(&breach);
    const solve_breach_time = timer.lap();

    std.debug.print("\nargs: {}\nfile: {}\nsolve: {}\ntotal: {}\n checks/s: {d}\n", .{
        std.fmt.fmtDuration(parse_args_time),
        std.fmt.fmtDuration(read_breach_time),
        std.fmt.fmtDuration(solve_breach_time),
        std.fmt.fmtDuration(parse_args_time + read_breach_time + solve_breach_time),
        @as(f32, @floatFromInt(breach.checks)) / (@as(f32, @floatFromInt(solve_breach_time)) / std.time.ns_per_s),
    });
}
